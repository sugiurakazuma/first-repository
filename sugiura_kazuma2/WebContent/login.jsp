<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/login.css" rel="stylesheet"type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">


				<c:forEach items="${errorMessages}" var="message">
						<c:out value="${message}" />
					</c:forEach>


			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="login" method="post">
			<br /><div class="name"> <label for="name">名前</label></div>
			 <input name="login_id" id="login_id" value="${login_id}" /> <br />
			  <label for="password">パスワード</label>
			   <input name="password" type="password" id="password" /> <br />
			    <input type="submit" value="ログイン" /> <br />

		</form>
	</div>
	 <div class="copyright"> Copyright(c)KazumaSugiura</div>
</body>
</html>