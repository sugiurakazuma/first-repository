<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">


<script type="text/javascript">
<!--

function disp(){

	if(window.confirm('削除しますか？')){
	return true;;
	}
	else{
	return false;
	}
}
// -->
</script>
<title>掲示板システム</title>
<link href="./css/top.css" rel="stylesheet"type="text/css">
</head>
<body>
	<div class="main-contents">

	<c:if test="${not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${message }" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">あ</a>
			</c:if>

			<c:if test="${ not empty loginUser }">
				<c:if test="${loginUser.position_id == 0}">

					<a href="managment">ユーザー管理</a>

				</c:if>
			<c:if test="${loginUser.position_id == 1}">
				<a class="managment" href="managment">管理画面</a>
			</c:if>

				<a class="new_contribution" href="new_contribution">新規投稿</a>



				<a class="logout" href="logout">ログアウト</a>
				<script>
				$(function() {
					  $(".logout").mouseover(function(){
					    $(this).css("background-color","green");
					  }).mouseout(function(){
					    $(this).css("background-color","");
					  });


					  $('.new_contribution').mouseover(function(){
							$(this).css('background-color','#CCFFFF');
						}).mouseout(function(){
						    $(this).css("background-color","");
						  });
				});

				$(".managment").on('mouseover',function(){
					$(this).css("background-color","white");
				});


				</script>






				<div class="title">
				<c:out value="社内掲示板"/>
				</div>



				<div class="name">
					<div class="welcome">
					<c:out value="ようこそ"/>
					<c:out value="${loginUser.name }" />
					<c:out value="さん"/>
					</div>
					<div class="menuber">
					<form action="./" method="get">

						<p>～日付絞込み～</p>  <p><input type="date" name="start" value="${start}">から <input type="date" name="end" value="${end}">まで</p>
						<br />
							 <p>～カテゴリ検索～</p> <p><input name="inputCategory" value="${inputCategory }" id="inputCategory" /></p>
							 <c:remove var="inputCategory" scope="session" />
						<p><input type="submit" value="検索"></p>

					</form>
					<form action="./" method="get">
					<input type="submit" value="リセット">
					</form>
					</div>
				</div>
				<div class="messages">
					<c:forEach var="message" items="${messages }">
						<div class="message">
							<div class="name">
								<span class="name"><c:out value="${message.name} さんの投稿" /></span>
							</div>
							<div class="subject_sub">

							</div>
							<div class="subject">
								<div class="subject_text">
								<c:out value="${message.subject }" />
								</div>
							</div>
							<div class="text">
							<c:forEach var="str" items="${ fn:split(message.text,'
										') }" >

										<c:out value="${str}" /><br>
							</c:forEach>
							</div>
							<div class="category">
								<c:out value="カテゴリー:${message.category }" />
							</div>
							<div class="date">
								<fmt:formatDate value="${message.created_at}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
							<c:if test="${message.user_id == loginUser.id}" var="flg" />
							<c:if test="${flg}">
								<form action="delete" method="post">
									<input name="message.id" value="${message.id }" id="message.id"
										type="hidden" /> <input type="submit" value="投稿削除" onClick="return disp()">


								</form>

							</c:if>
							</div>
							<c:forEach var="comment" items="${comments }">
								<c:if test="${comment.message_id == message.id}" var="flg2" />
								<c:if test="${flg2 }">

									<c:out value="${comment.name } さんのコメント" />

									<div class="comment">
									<br/>
										<div class="commenttext">
										<c:forEach var="str" items="${ fn:split(comment.text,'
										') }" >
										<c:out value="${str}" /><br>
										</c:forEach>
										</div>


									<div class="date">
										<fmt:formatDate value="${comment.created_at}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
									<c:if test="${comment.user_id == loginUser.id}" var="flg" />
									<c:if test="${flg}">
										<form action="commentdelete" method="post">
										<div class="date">
										<fmt:formatDate value="${comment.created_at}"
											pattern="yyyy/MM/dd HH:mm:ss" />
										</div>
											<input name="comment.id" value="${comment.id }"type="hidden" />
											<input type="submit"value="コメント削除" onClick="return disp()">
										</form>
									</c:if>

									</div>
								</c:if>
							</c:forEach>




							<form action="new_comment" method="post">
							<input name="message_id" value="${message.id }" id="message_id" type="hidden" />
								 <input name="login_id" value="${loginUser.id }" id="login_id" type="hidden" />
								<textarea name="text" cols="50" rows="5" class="text-box"><c:if test="${comment_message_id == message.id}">${text}</c:if></textarea>

								<br /> <input name="message_id" value="${message.id }" id="message_id" type="hidden" />
								 <input name="login_id" value="${loginUser.id }" id="login_id" type="hidden" />
									<br />
									<input type="submit" value="コメントする">
							</form>


					</c:forEach>
					<c:remove var="text" scope="session" />

				</div>

			</c:if>

		</div>
	</div>
	 <div class="copyright"> Copyright(c)KazumaSugiura</div>
<script type="text/javascript">
				$('logout').hover(function(){
							$('logout').css('background-color','#00f');
						});

				</script>
</body>
</html>