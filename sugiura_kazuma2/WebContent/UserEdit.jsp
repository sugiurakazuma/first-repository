<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <link href="./css/EditUser.css" rel="stylesheet"type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー編集</title>
    </head>
    <body>
        <div class="main-contents">
        <a class="return" href="managment">←管理画面へ戻る</a>
        <c:if test="${not empty errorMessages }">
        	<div class="errorMessages">
        		<ul>
        			<c:forEach items="${errorMessages }" var="message">
        				<li><c:out value="${message }" />
        			</c:forEach>
        	</ul>
 			</div>
 			<c:remove var="errorMessages" scope="session"/>
 		</c:if>

 		<form action="settings" method="post"><br />
			<div class="name">
 			<input name="id" value="${editUser.id}" id="id" type="hidden"/>
 			<label for="name">名前(10文字以下)</label><br />
 			<input name="name" value="${editUser.name }" id="name"/><br />

 			<label for="login_id">ログインID(6文字以上20文字以内)</label><br />
 			<input type="hidden" name="CheckId" value="${editUser.login_id }" />
 			<input name="login_id" value="${editUser.login_id }" id="login_id"/><br />

 			<label for="password">パスワード(6文字以上20文字以内)</label><br />
 			<input name="password" type="password" id="password"/><br />

			<label for="password2">パスワード(確認用)(6文字以上20文字以内)</label><br />
			<input name="password2" type="password" id="password2" /> <br />
			<c:if test="${loginUser.id == editUser.id }">

			<c:forEach var="Branches" items="${Branches}">
				<c:if test="${loginUser.branch_id == Branches.id }">
				支店:<c:out value="${Branches.name}" />
				<input name="branch_id" value="${Branches.id }" id="branch_id"
										type="hidden" />
				</c:if>

			</c:forEach>


			<c:forEach var="Positions" items="${Positions}">


				<c:if test="${loginUser.position_id == Positions.id }">
				役職:<c:out value="${Positions.name}" />
				<input name="position_id" value="${Positions.id }" id="position_id"
										type="hidden" />
				</c:if>

			</c:forEach>
			</c:if>
			<c:if test="${loginUser.id != editUser.id }">
			<select name="branch_id">
			<c:forEach var="Branches" items="${Branches}">
			<c:choose>

			<c:when test="${Branches.id == editUser.branch_id}">
			  支店:<option value="${Branches.id}"selected>${Branches.name}</option>
			   </c:when>
			   <c:otherwise>
			   支店:<option value="${Branches.id}">${Branches.name}</option>
			   </c:otherwise>

			</c:choose>
			</c:forEach>
			</select>
			</c:if>
			<c:if test="${loginUser.id != editUser.id }">
				<select name="position_id">
			<c:forEach var="Positions" items="${Positions}">
			<c:choose>
			<c:when test="${Positions.id == editUser.position_id}">

			  役職:<option value="${Positions.id}"selected>${Positions.name}</option>
			   </c:when>
			   <c:otherwise>
			   役職:<option value="${Positions.id}">${Positions.name}</option>
			   </c:otherwise>
			   </c:choose>
			</c:forEach>

			</select>
			</c:if>






 			<input type="submit" value="登録" /><br />
	</div>
 		</form>
 		</div>
 		 <div class="copyright"> Copyright(c)KazumaSugiura</div>
 		</body>
 		</html>
