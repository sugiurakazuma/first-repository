<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/new_contribution.css" rel="stylesheet"type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規登録</title>
</head>
<body>

	<div class="main-contents">
		<c:if test="${not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages }" var="message">
						<li><c:out value="${message }" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="new_contribution" method="post">
			<a class="return" href="./">←トップへ戻る</a> <br />
			<div class="subject">
			<label for="subject">件名</label>
			<input name="subject" value="${subject}" id="subject"/><br />
			</div>
			<div class="category">
			<label for="category">カテゴリ</label>
			<input name="category" value="${category}" id="category"/><br />
			</div>
			<div class="text">
			<label for="text">本文</label>
			<textarea name="text" cols="50" rows="5" class="text-box">${text}</textarea><br />
			</div>
			<c:remove var="subject" scope="session" />
			<c:remove var="category" scope="session" />
			<c:remove var="text" scope="session" />
			<div class="push">
			<input type="submit" value="投稿する"><br />
			</div>
		</form>
	</div>
 <div class="copyright"> Copyright(c)KazumaSugiura</div>
</body>
</html>
