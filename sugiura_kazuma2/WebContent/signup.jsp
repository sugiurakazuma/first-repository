<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/signup.css" rel="stylesheet"type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br />
			 <a class="return" href="managment">←管理画面へ戻る</a>
			<div class="name"><label for="name">名前(10文字以下)</label>
			<input name="name" value="${user.name}" id="name" /><br />
			<label for="login_id">ログインID(6文字以上20文字以内)</label>
			<input name="login_id" value="${user.login_id}" id="login_id" /><br />
			<label for="password">パスワード(6文字以上20文字以内)</label>
			<input name="password" type="password" id="password" /> <br />
			<label for="password2">パスワード(確認用)(6文字以上20文字以内)</label>
			<input name="password2" type="password" id="password2" /> <br />

			<select name="branch_id">
			<c:forEach var="Branches" items="${Branches}">
			<c:choose>
			<c:when test="${Branches.id == user.branch_id}">
			  <option value="${Branches.id}"selected>${Branches.name}</option>
			   </c:when>
			   <c:otherwise>
			   <option value="${Branches.id}">${Branches.name}</option>
			   </c:otherwise>




			</c:choose>
			</c:forEach>
			</select>


			<select name="position_id">
			<c:forEach var="Positions" items="${Positions}">
			<c:choose>
			<c:when test="${Positions.id == user.position_id}">
			  <option value="${Positions.id}"selected>${Positions.name}</option>
			   </c:when>
			   <c:otherwise>
			   <option value="${Positions.id}">${Positions.name}</option>
			   </c:otherwise>
			   </c:choose>
			</c:forEach>

			</select>



			  <input type="submit" value="登録" />
			<br />
			</div>




		</form>
	</div>
	 <div class="copyright"> Copyright(c)KazumaSugiura</div>
</body>
</html>