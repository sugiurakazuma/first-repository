<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/managment.css" rel="stylesheet"type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">


function disp1(){
	if(window.confirm('停止しますか？')){
	return true;;
	}
	else{
	return false;
	}
}


function disp0(){
	if(window.confirm('再開しますか？')){
	return true;;
	}
	else{
	return false;
	}
}

</script>
<title>ユーザー管理</title>
</head>
<body>
	<div class="main-contents">
	<c:if test="${not empty errorMessages }">
        	<div class="errorMessages">
        		<ul>
        			<c:forEach items="${errorMessages }" var="message">
        				<li><c:out value="${message }" />
        			</c:forEach>
        	</ul>
 			</div>
 			<c:remove var="errorMessages" scope="session"/>
 		</c:if>
		<div class="managment_table">

			<a class="return" href="./">←トップへ戻る</a> <br /> <a class="signup" href="signup">ユーザー新規登録</a><br />

		<table class="table" border="2" align="center" >
			<tr>

			<th><c:out value="名前" /></th>
			<th><c:out value="ログインID" /></th>
			<th><c:out value="支店" /></th>
			<th><c:out value="役職" /></th>
			<th><c:out value="ユーザー状況" /></th>
			<th><c:out value="編集"/></th>
			</tr>



			<c:forEach var="users" items="${ users }">

				<tr>

				<td><span class=name><c:out value="${users.name }" /></span></td>
				<td><span class=login_id><c:out value="${users.login_id }" /></span></td>
				<td><span class=branch_id><c:out value="${users.branch_name }" /></span></td>
				<td><span class=position_id><c:out value="${users.position_name }" /></span></td>


				<c:if test="${users.id == loginUser.id}">
				<td>ログイン中</td>
				</c:if>

				<c:if test="${users.id != loginUser.id}">

				<c:if test= "${users.is_deleted == 1}">

				<form action="is_deleted" method="post">
					<input name="id" value="${users.id }"type="hidden" />
					<input name="is_deleted" value="${users.is_deleted }"type="hidden" />
					<td><input type="submit" value="再開" onClick="return disp0()"/></td>

				</form>
				</c:if>

				<c:if test= "${users.is_deleted == 0}">
				<form action="is_deleted" method="post">
					<input name="id" value="${users.id }"type="hidden" />
					<input name="is_deleted" value="${users.is_deleted }"type="hidden" />
					<td><input type="submit" value="停止"onClick="return disp1()" /></td>

				</form>
				</c:if>
				</c:if>


				<form action="settings" method="get">
				<input name="id" value="${users.id }"type="hidden" />
				<td><input type="submit" value="編集" /></td>
				</tr>
				</form>

			</c:forEach>

			</table>
		</div>
	</div>
	 <div class="copyright"> Copyright(c)KazumaSugiura</div>
</body>
</html>
