package beans;

public class Users {
	private String id;
	private String name;
	private String login_id;
	private String branch_id;
	private String position_id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

	public String getPosition_id() {
		return position_id;
	}

	public void setPosition_id(String position_id) {
		this.position_id = position_id;
	}

	public Users(String id, String name, String login_id, String branch_id, String position_id) {
		this.id = id;
		this.name = name;
		this.login_id = login_id;
		this.branch_id = branch_id;
		this.position_id = position_id;
	}

}
