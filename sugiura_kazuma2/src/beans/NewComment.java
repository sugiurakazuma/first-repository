package beans;

import java.io.Serializable;

import javax.xml.crypto.Data;

public class NewComment implements Serializable {
	private static final long serialVarsionUID = 1L;

	private int id;
	private String text;
	private Data created_at;
	private Data updated_at;
	private int user_id;
	private int message_id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Data getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Data created_at) {
		this.created_at = created_at;
	}

	public Data getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Data updated_at) {
		this.updated_at = updated_at;
	}

	public static long getSerialvarsionuid() {
		return serialVarsionUID;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getMessage_id() {
		return message_id;
	}

	public void setMessage_id(int message_id) {
		this.message_id = message_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
