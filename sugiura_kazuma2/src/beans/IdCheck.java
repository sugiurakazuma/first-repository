package beans;

import java.io.Serializable;

public class IdCheck implements Serializable {
	private static final long serialVarsionUID = 1L;

	private int id;
	private String login_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public static long getSerialvarsionuid() {
		return serialVarsionUID;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
}

