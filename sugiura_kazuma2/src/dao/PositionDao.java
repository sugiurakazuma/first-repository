package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.PositionBeans;
import exception.SQLRuntimeException;

public class PositionDao {


public List<PositionBeans> getPositionList(Connection connection, int limitNum) {

	PreparedStatement ps = null;
	try {
		String sql = "SELECT * FROM sugiura_kazuma.positions";

		ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		List<PositionBeans> PositionList = toUserList(rs);

		return PositionList;
	} catch (SQLException e) {
		throw new SQLRuntimeException(e);
	} finally {
		close(ps);
	}


}
private List<PositionBeans> toUserList(ResultSet rs) throws SQLException {

	List<PositionBeans> ret = new ArrayList<PositionBeans>();
	try {
		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");

			PositionBeans Positions = new PositionBeans();
			Positions.setId(id);
			Positions.setName(name);

			ret.add(Positions);
		}
		return ret;
	} finally {
		close(rs);
	}
}
}