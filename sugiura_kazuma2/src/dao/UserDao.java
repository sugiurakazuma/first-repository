package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User users) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("name");
			sql.append(",login_id");
			sql.append(", password");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_deleted");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", 0");

			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, users.getName());
			ps.setString(2, users.getLogin_id());
			ps.setString(3, users.getPassword());
			ps.setString(4, users.getBranch_id());
			ps.setString(5, users.getPosition_id());
			//ps.setInt(6, users.getIs_deleted());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String branch_id = rs.getString("branch_id");
				String position_id = rs.getString("position_id");
				int Is_deleted = rs.getInt("Is_deleted");

				User users = new User();
				users.setId(id);
				users.setName(name);
				users.setLogin_id(login_id);
				users.setPassword(password);
				users.setBranch_id(branch_id);
				users.setPosition_id(position_id);
				users.setIs_deleted(Is_deleted);


				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public User getUser(Connection connection, String login_id, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT *  from users where login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, login_id);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList2(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String branch_id = rs.getString("branch_id");
				String position_id = rs.getString("position_id");
				String branch_name = rs.getString("branch_name");
				String position_name = rs.getString("position_name");
				int is_deleted = rs.getInt("is_deleted");
				User users = new User();
				users.setId(id);
				users.setName(name);
				users.setLogin_id(login_id);
				users.setPassword(password);
				users.setBranch_id(branch_id);
				users.setPosition_id(position_id);
				users.setBranch_name(branch_name);
				users.setPosition_name(position_name);
				users.setIs_deleted(is_deleted);

				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<User> getUserList(Connection connection) {

		PreparedStatement ps = null;
		try {
			//String sql = "SELECT * FROM sugiura_kazuma.users";

			StringBuilder sql1 = new StringBuilder();
			sql1.append("SELECT ");
			sql1.append("users.id as id,");
			sql1.append("users.name as name, ");
			sql1.append("users.password as password, ");
			sql1.append("users.login_id as login_id,");
			sql1.append("users.branch_id as branch_id,");
			sql1.append("users.position_id as position_id,");
			sql1.append("users.is_deleted as is_deleted, ");
			//sql1.append("users.created_at as created_at ");
			sql1.append("branches.id as branch_id, ");
			sql1.append("branches.name as branch_name, ");
			sql1.append("positions.id as position_id, ");
			sql1.append("positions.name as position_name ");
			sql1.append("FROM users ");
			sql1.append("INNER JOIN branches ");
			sql1.append("ON users.branch_id = branches.id ");
			sql1.append("INNER JOIN positions ");
			sql1.append("ON users.position_id = positions.id ");
			sql1.append("ORDER BY branches.id ASC");




			ps = connection.prepareStatement(sql1.toString());
			System.out.println(ps.toString());
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList2(rs);

			return userList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}


	}


	public User getUser(Connection connection, int Sum) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, Sum);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user,String password) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE  users  SET  ");
			sql.append("login_id = ?");
			sql.append(", name = ?");
			if(StringUtils.isBlank(password) != true){
			sql.append(", password = ?");
			}
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			if(StringUtils.isBlank(password) != true){
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getBranch_id());
			ps.setString(5, user.getPosition_id());
			ps.setInt(6, user.getId());
			}else{


			ps.setString(3, user.getBranch_id());
			ps.setString(4, user.getPosition_id());
			ps.setInt(5, user.getId());

			}
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}









}