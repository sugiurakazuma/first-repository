package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class Is_DeletedDao {

	public void is_deleted(Connection connection, int userId,int userIsDeleted) {

		PreparedStatement ps = null;

		try {


			if(userIsDeleted == 0){
				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET is_deleted = '1'  ");
				sql.append(" WHERE");
				sql.append(" id = ?");
				ps = connection.prepareStatement(sql.toString());


				ps.setInt(1, userId);
				ps.executeUpdate();
			}
			else{
				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET is_deleted = '0' ");
				sql.append(" WHERE");
				sql.append(" id = ?");
				ps = connection.prepareStatement(sql.toString());

				ps.setInt(1, userId);
				ps.executeUpdate();
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}