package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.NewMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<NewMessage> getUserMessages(Connection connection, int num, String start, String end,
			String inputCategory) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT ");
			sql.append("contributiones.id as id,");
			sql.append("contributiones.subject as subject,");
			sql.append("contributiones.category as category,");
			sql.append("contributiones.text as text,");
			sql.append("contributiones.user_id as user_id,");
			sql.append("users.login_id as login_id,");
			sql.append("users.name as name,");
			sql.append("contributiones.created_at as created_at ");
			sql.append("FROM contributiones ");
			sql.append("INNER JOIN users ");
			sql.append("ON contributiones.user_id = users.id ");
			sql.append("WHERE created_at BETWEEN ? AND ? ");
			if (StringUtils.isEmpty(inputCategory) == false) {
				sql.append(" AND category LIKE  ? ");
			}
			sql.append(" ORDER BY contributiones.created_at DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());


			if (StringUtils.isEmpty(start) == true && StringUtils.isBlank(end) == true) {
				ps.setString(1, "2000-01-01");
				ps.setString(2, "3000-01-01 23:59:59");

			} else if (StringUtils.isBlank(start) == false && StringUtils.isBlank(end) == true) {
				ps.setString(1, start);
				ps.setString(2, "3000-01-01 23:59:59");
			} else if (StringUtils.isEmpty(start) == true && StringUtils.isBlank(end) == false) {
				ps.setString(1, "2000-01-01");
				ps.setString(2, end + " 23:59:59");
			} else {
				ps.setString(1, start);
				ps.setString(2, end + " 23:59:59");
			}
			if (StringUtils.isEmpty(inputCategory) == false) {
				ps.setString(3, "%" + inputCategory + "%");
			}


			ResultSet rs = ps.executeQuery();
			List<NewMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<NewMessage> toUserMessageList(ResultSet rs) throws SQLException {

		List<NewMessage> ret = new ArrayList<NewMessage>();
		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String name = rs.getString("name");
				String subject = rs.getString("subject");
				String category = rs.getString("category");
				String text = rs.getString("text");
				Timestamp created_at = rs.getTimestamp("created_at");
				Integer user_id = rs.getInt("user_id");

				NewMessage message = new NewMessage();

				message.setName(name);
				message.setId(id);
				message.setSubject(subject);
				message.setCategory(category);
				message.setText(text);
				message.setCreated_at(created_at);
				message.setUser_id(user_id);

				ret.add(message);

			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
