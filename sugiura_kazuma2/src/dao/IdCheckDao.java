package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class IdCheckDao{


	private List<User> toUserList2(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				String login_id = rs.getString("login_id");
				User users = new User();
				users.setLogin_id(login_id);
				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public boolean getUser(Connection connection,String login_id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM sugiura_kazuma.users where login_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList2(rs);

			if (userList.isEmpty() == true) {
				return true;
			}// else if (2 <= userList.size()) {
				//throw new IllegalStateException("2 <= userList.size()");
			//}
		else {
				return false;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}



	}
