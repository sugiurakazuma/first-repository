package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.NewComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<NewComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id,");
			sql.append("comments.text as text,");
			sql.append("comments.user_id as user_id,");
			sql.append("comments.message_id as message_id,");
			sql.append("users.login_id as login_id,");
			sql.append("users.name as name,");
			sql.append("comments.created_at as created_at ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_at ASC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<NewComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<NewComment> toUserCommentList(ResultSet rs) throws SQLException {

		List<NewComment> ret = new ArrayList<NewComment>();
		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String text = rs.getString("text");
				Integer user_id = rs.getInt("user_id");
				int message_id = rs.getInt("message_id");
				String name = rs.getString("name");
				//Timestamp created_at = rs.getTimestamp("created_at");


				NewComment comment = new NewComment();

				comment.setId(id);
				comment.setText(text);
				comment.setUser_id(user_id);
				comment.setMessage_id(message_id);
				comment.setName(name);
				//comment.setCreated_at(created_at);

				ret.add(comment);

			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
