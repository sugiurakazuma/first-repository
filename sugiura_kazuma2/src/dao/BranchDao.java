package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.BranchBeans;
import exception.SQLRuntimeException;

public class BranchDao {


public List<BranchBeans> getBranchList(Connection connection, int limitNum) {

	PreparedStatement ps = null;
	try {
		String sql = "SELECT * FROM sugiura_kazuma.branches";

		ps = connection.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		List<BranchBeans> BranchList = toUserList(rs);

		return BranchList;
	} catch (SQLException e) {
		throw new SQLRuntimeException(e);
	} finally {
		close(ps);
	}


}
private List<BranchBeans> toUserList(ResultSet rs) throws SQLException {

	List<BranchBeans> ret = new ArrayList<BranchBeans>();
	try {
		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");

			BranchBeans Branches = new BranchBeans();
			Branches.setId(id);
			Branches.setName(name);

			ret.add(Branches);
		}
		return ret;
	} finally {
		close(rs);
	}
}
}
