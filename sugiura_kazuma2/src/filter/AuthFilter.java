package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter("/*")
public class AuthFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String target = ((HttpServletRequest) request).getRequestURI();


		HttpSession session = ((HttpServletRequest) request).getSession();
		if (!(target.contains("managment")) ) { 	//urlに"managment"が含まれている場合
		}else{


			if (session == null) {


				((HttpServletResponse)response).sendRedirect("./");
				return;
			} else {
				User user = (User) session.getAttribute("loginUser");
				if (user == null) {
					chain.doFilter(request,response);
					return;
				}

				int auth	= Integer.parseInt(user.getPosition_id());

				if (auth != 1) {
					String filtermessage=("権限がありません");
					session.setAttribute("errorMessages", filtermessage);
				((HttpServletResponse)response).sendRedirect("./");
				return;
				}

			}
		}
		chain.doFilter(request, response);


	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

}
