package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.CommentDeleteDao;


public class CommentDeleteService {

    public void register(int commentDelete) {

        Connection connection = null;
        try {
            connection = getConnection();



            CommentDeleteDao deleteDao = new CommentDeleteDao();
            deleteDao.delete(connection, commentDelete);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}