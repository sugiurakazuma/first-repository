package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.Is_DeletedDao;


public class Is_DeletedService {

    public void register(int userId,int userIsDeleted) {

        Connection connection = null;
        try {
            connection = getConnection();



            Is_DeletedDao deleteDao = new Is_DeletedDao();
            deleteDao.is_deleted(connection, userId,userIsDeleted);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}