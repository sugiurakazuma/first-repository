package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Message;
import dao.Subject_MessageDao;

public class Subject_MessageService{
	public void register(Message massege){
		Connection connection = null;
		try{
			connection = getConnection();

			Subject_MessageDao Subject_messageDao = new Subject_MessageDao();
			Subject_messageDao.insert(connection,massege);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;

		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
}