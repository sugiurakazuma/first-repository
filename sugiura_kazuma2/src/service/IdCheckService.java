package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.IdCheckDao;

public class IdCheckService{

	public static boolean CheckId(String Login_id) {

        Connection connection = null;
        try {
            connection = getConnection();



            IdCheckDao idCheckDao = new IdCheckDao();
            boolean Check = idCheckDao.getUser(connection,Login_id);

            commit(connection);
            return Check;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }


}



