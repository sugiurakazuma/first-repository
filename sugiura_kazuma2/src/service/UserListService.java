package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;

public class UserListService {

    public List<User> getUserList() {


        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();

           List<User> users = userDao.getUserList(connection);

            commit(connection);

            return users;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}