package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.NewMessage;
import dao.MessageDao;
import dao.UserMessageDao;


public class MessageService {

    public void register(NewMessage new_message) {

        Connection connection = null;
        try {
            connection = getConnection();



            MessageDao userDao = new MessageDao();
            userDao.insert(connection, new_message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<NewMessage>getMessage(String start,String end,String inputCategory){
    	Connection connection = null;
    	try{
    		connection = getConnection();

    		UserMessageDao messageDao = new UserMessageDao();
    		List<NewMessage> ret = messageDao.getUserMessages(connection,LIMIT_NUM ,start,end,inputCategory);
    		commit(connection);
    		return ret;
    	}catch(RuntimeException e){
    		rollback(connection);
    		throw e;
    	}catch(Error e){
    		rollback(connection);
    		throw e;
    	}finally{
    		close(connection);
    	}
    }
}