package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.BranchBeans;
import beans.PositionBeans;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.IdCheckService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<BranchBeans> Branches = new BranchService().getBranch();
		List<PositionBeans> Positions = new PositionService().getPosition();

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			int Sum = Integer.parseInt(request.getParameter("id"));
			User editUser = new UserService().getUser(Sum);
			request.setAttribute("Branches", Branches);
			request.setAttribute("Positions", Positions);

			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("UserEdit.jsp").forward(request, response);
		} else {

			session.setAttribute("errorMessages", messages);
			System.out.println(request.getParameter("id"));

//			request.getRequestDispatcher("managment").forward(request, response);
			response.sendRedirect("managment");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages2 = new ArrayList<String>();
		HttpSession session = request.getSession();

		User editUser = getEditUser(request);

		if (isValid2(request, messages2) == true) {

			try {
				new UserService().update(editUser, editUser.getPassword());
			} catch (NoRowsUpdatedRuntimeException e) {

				request.getRequestDispatcher("managment").forward(request, response);
				return;
			}

			if (session.getAttribute("id") == request.getParameter("id")) {
				session.setAttribute("loginUser", editUser);
			}
			response.sendRedirect("managment");
		} else {
			session.setAttribute("errorMessages", messages2);
			// int Sum = Integer.parseInt(request.getParameter("id"));
			// System.out.println(request.getParameter("id"));
			List<BranchBeans> Branches = new BranchService().getBranch();
			List<PositionBeans> Positions = new PositionService().getPosition();
			// User user = new User();
			// user.setName(request.getParameter("name"));
			// user.setLogin_id(request.getParameter("login_id"));

			request.setAttribute("Branches", Branches);
			request.setAttribute("Positions", Positions);

			// request.setAttribute("user",user);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("/UserEdit.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch_id(request.getParameter("branch_id"));
		editUser.setPosition_id(request.getParameter("position_id"));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		if (request.getParameter("id") != null) { // nullじゃない
			if ((request.getParameter("id").matches("^[0-9]{1,4}$"))) {
				int Sum = Integer.parseInt(request.getParameter("id"));
				User editUser = new UserService().getUser(Sum);
				if (editUser == null) {
					messages.add("不正なパラメーターです");
				}
			} else {
				messages.add("不正なパラメーターです");
			}

		} else {
			messages.add("不正なパラメーターです");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isValid2(HttpServletRequest request, List<String> messages2) {

		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String CheckId = request.getParameter("CheckId");
		//User editUser = new User();
		//new UserService().update(editUser,password);


		if (StringUtils.isBlank(name) == true) {
			messages2.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages2.add("名前は10文字以内で入力してください");
		}
		if (StringUtils.isBlank(login_id) == true) {
			messages2.add("アカウント名を入力してください");
		} else if (login_id.matches("[0-9a-zA-Z]{6,20}") != true) {
			messages2.add("ログインIDは6文字以上20文字以内で入力してください");
		}else if(CheckId.equals(login_id)){
			//messages2.add("idが重複しています");
		}




		else if(IdCheckService.CheckId(login_id) != true) {
			messages2.add("IDが重複しています");
		}


		if(!StringUtils.isBlank(password) && !StringUtils.isBlank(password2)){
			if (!(password.equals(password2))){
				messages2.add("パスワードが一致しません");
			}
			if (password.matches("[0-9a-zA-Z]{6,20}") != true) {
				messages2.add("パスワードは6文字以上20文字以下で入力してください");
			}
		}
		if(StringUtils.isBlank(password) && !StringUtils.isBlank(password2)){
			if (password2.matches("[0-9a-zA-Z]{6,20}") != true) {
				messages2.add("パスワードは6文字以上20文字以下で入力してください");
			}
		}
		if(!StringUtils.isBlank(password) && StringUtils.isBlank(password2)){
			if (password.matches("[0-9a-zA-Z]{6,20}") != true) {
				messages2.add("パスワードは6文字以上20文字以下で入力してください");
			}
		}
		if (messages2.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}


