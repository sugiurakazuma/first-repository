package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.BranchBeans;
import beans.PositionBeans;
import beans.User;
import service.BranchService;
import service.IdCheckService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//request.getRequestDispatcher("signup.jsp").forward(request, response);

		List<BranchBeans> Branches = new BranchService().getBranch();
		List<PositionBeans> Positions = new PositionService().getPosition();

		request.setAttribute("Branches",Branches);



		request.setAttribute("Positions",Positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);


	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setName(request.getParameter("name"));
			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setPassword2(request.getParameter("password2"));
			user.setBranch_id(request.getParameter("branch_id"));
			user.setPosition_id(request.getParameter("position_id"));

			new UserService().register(user);

			response.sendRedirect("managment");
		} else {
			User user = new User();
			user.setName(request.getParameter("name"));
			user.setLogin_id(request.getParameter("login_id"));
			user.setBranch_id(request.getParameter("branch_id"));
			user.setPosition_id(request.getParameter("position_id"));

			session.setAttribute("errorMessages", messages);
			List<BranchBeans> Branches = new BranchService().getBranch();
			List<PositionBeans> Positions = new PositionService().getPosition();


			request.setAttribute("user",user);
			request.setAttribute("Branches",Branches);
			request.setAttribute("Positions",Positions);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String branch_id = request.getParameter("branch_id");
		String position_id = request.getParameter("position_id");

		System.out.println(IdCheckService.CheckId(login_id));

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");
		} else if ( login_id.length() < 6  ) {
			messages.add("ログインIDは6文字以上20文字以内で入力してください");

		}else if(login_id.length() > 20){
			messages.add("ログインIDは6文字以上20文字以内で入力してください");
		}

		else if(!login_id.matches("[0-9a-zA-Z]+")){
			messages.add("パスワードの入力定義に従っていません");
		}else if(IdCheckService.CheckId(login_id) != true) {
			messages.add("IDが重複しています");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (password.length() <= 6 || password.length() > 20 ) {
			messages.add("パスワードは6文字以上20文字以内で入力してください");
		}
		if (StringUtils.isEmpty(password2) == true) {
			messages.add("確認用パスワードを入力してください");
		} else if (password2.length() <= 6 || password2.length() > 20 ) {
			messages.add("確認用パスワードは6文字以上20文字以内で入力してください");
		}

		if (!password.equals(password2)){
			messages.add("パスワードが一致しません");
		}
		if (StringUtils.isEmpty(branch_id) == true) {
			messages.add("支店を入力してください");
		}
		if (StringUtils.isEmpty(position_id) == true) {
			messages.add("役職を入力してください");
		}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}
}
