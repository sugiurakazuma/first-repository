package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.NewComment;
import service.NewCommentService;

@WebServlet(urlPatterns = { "/new_comment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {

			NewComment comment = new NewComment();

			comment.setText(request.getParameter("text"));
			comment.setUser_id(Integer.parseInt(request.getParameter("login_id")));
			comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));



			new NewCommentService().register(comment);

			response.sendRedirect("./");
		} else {
			String text = request.getParameter("text");
			String login_id = request.getParameter("login_id");
			String message_id = request.getParameter("message_id");
			session.setAttribute("text",text);
			session.setAttribute("login_id",login_id);
			session.setAttribute("comment_message_id",message_id);
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String text = request.getParameter("text");

		if (StringUtils.isBlank(text) == true) {
			messages.add("コメントを入力してください");
		}else if(text.length() > 500){
			System.out.println("コメント"+text.length());
			messages.add("500文字以内で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}
}
