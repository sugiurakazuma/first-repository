package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CommentDeleteService;

@WebServlet(urlPatterns = { "/commentdelete" })
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;




	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		//List<String> messages = new ArrayList<String>();
		//HttpSession session = request.getSession();
		//if (isValid(request, messages) == true) {

			int commentDelete = Integer.parseInt(request.getParameter("comment.id"));
			System.out.println(Integer.parseInt(request.getParameter("comment.id")));
			new CommentDeleteService().register(commentDelete);

			response.sendRedirect("./");
		} /*else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}*/

	//}

	//private boolean isValid(HttpServletRequest request, List<String> messages) {

		//if (messages.size() == 0) {
			//return true;
		//} else {
			//return false;
		//}

	}
//}
