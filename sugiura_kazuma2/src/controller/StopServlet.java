//
//package controller;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import org.apache.commons.lang.StringUtils;
//
//import beans.User;
//import exception.NoRowsUpdatedRuntimeException;
//import service.UserService;
//
//@WebServlet(urlPatterns = { "/" })
//public class StopServlet extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//
//	@Override
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//
//		HttpSession session = request.getSession();
//
//		User loginUser = (User) session.getAttribute("loginUser");
//
//		User editUser = new UserService().getUser(loginUser.getId());
//		request.setAttribute("editUser", editUser);
//
//		request.getRequestDispatcher("managment.jsp").forward(request, response);
//	}
//
//	@Override
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//
//		List<String> messages = new ArrayList<String>();
//		HttpSession session = request.getSession();
//		User editUser = getEditUser(request);
//
//		if (isValid(request, messages) == true) {
//
//			try {
//				new UserService().update(editUser);
//			} catch (NoRowsUpdatedRuntimeException e) {
//				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
//				session.setAttribute("errorMessages", messages);
//				request.setAttribute("editUser", editUser);
//				request.getRequestDispatcher("managment.jsp").forward(request, response);
//				return;
//			}
//
//			session.setAttribute("loginUser", editUser);
//
//			response.sendRedirect("./");
//		} else {
//			session.setAttribute("errorMessages", messages);
//			request.setAttribute("editUser", editUser);
//			request.getRequestDispatcher("managment.jsp").forward(request, response);
//		}
//	}
//
//	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {
//
//		User editUser = new User();
//
//		editUser.setName(request.getParameter("is_deleted"));
//
//		return editUser;
//	}
//
//	private boolean isValid(HttpServletRequest request, List<String> messages) {
//
//		String login_id = request.getParameter("login_id");
//		String password = request.getParameter("password");
//
//		if (StringUtils.isEmpty(login_id) == true) {
//			messages.add("アカウント名を入力してください");
//		}
//		if (StringUtils.isEmpty(password) == true) {
//			messages.add("パスワードを入力してください");
//		}
//
//		if (messages.size() == 0) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//}
