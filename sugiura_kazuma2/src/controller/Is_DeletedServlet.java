package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.Is_DeletedService;

@WebServlet(urlPatterns = { "/is_deleted" })
public class Is_DeletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			int  userId= Integer.parseInt(request.getParameter("id"));
			int  userIsDeleted=Integer.parseInt(request.getParameter("is_deleted"));


			new Is_DeletedService().register(userId,userIsDeleted);


			response.sendRedirect("managment");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("managment");
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}
}
