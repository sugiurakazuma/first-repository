package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.NewComment;
import beans.NewMessage;
import beans.User;
import service.MessageService;
import service.NewCommentService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;

		} else {
			isShowMessageForm = false;
		}

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String inputCategory = request.getParameter("inputCategory");

		session.setAttribute("start",start);
		session.setAttribute("end",end);
		session.setAttribute("inputCategory",inputCategory);

		List<NewMessage> messages = new MessageService().getMessage(start, end, inputCategory);
		List<NewComment> comments = new NewCommentService().getComment();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

}
