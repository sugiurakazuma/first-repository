package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.NewMessage;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/new_contribution" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.getRequestDispatcher("new_contribution.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();


		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");
			NewMessage new_message = new NewMessage();
			new_message.setSubject(request.getParameter("subject"));
			new_message.setCategory(request.getParameter("category"));
			new_message.setText(request.getParameter("text"));
			new_message.setUser_id(user.getId());

			new MessageService().register(new_message);

			response.sendRedirect("./");
		} else {
			String subject = request.getParameter("subject");
			String category = request.getParameter("category");
			String text = request.getParameter("text");

			session.setAttribute("subject",subject);
			session.setAttribute("category",category);
			session.setAttribute("text",text);
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("new_contribution");
		}

	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String subject = request.getParameter("subject");
		String category = request.getParameter("category");
		String text = request.getParameter("text");

		if (StringUtils.isBlank(subject) == true) {
			messages.add("件名を入力してください");
		}else if(subject.length() > 30){
			messages.add("30文字以内で入力してください");
		}
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリを入力してください");
		}else if(category.length() > 10){
			messages.add("10文字以内で入力してください");
		}
		if (StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}else if(text.length() > 1000){
			System.out.println(text.length());
			messages.add("1000文字以内で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}
}
