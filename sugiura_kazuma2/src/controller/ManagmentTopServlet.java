package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserListService;

@WebServlet(urlPatterns = { "/managment" })
public class ManagmentTopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		List<User> UserList = new UserListService().getUserList();

		request.setAttribute("users", UserList);


		request.getRequestDispatcher("/managment.jsp").forward(request, response);
	}
}
